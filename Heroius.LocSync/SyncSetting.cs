﻿using Heroius.Extension.WPF;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heroius.LocSync
{
    public class SyncSetting
    {
        [DataType("Folder"), Display(Name = "左侧")]
        public string Left { get; set; } = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
        [DataType("Folder"), Display(Name = "右侧")]
        public string Right { get; set; } = Environment.GetFolderPath(Environment.SpecialFolder.CommonDocuments);
    }
}
